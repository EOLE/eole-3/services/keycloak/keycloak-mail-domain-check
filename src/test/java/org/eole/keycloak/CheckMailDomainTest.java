package org.eole.keycloak;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;

public class CheckMailDomainTest
{

    @Test
    public void test01()
    {
        assertNotNull( CheckMailDomain.validateUsername("01") ); // 3 car err
        assertNull( CheckMailDomain.validateUsername("012") ); // 3 car ok
        assertNotNull( CheckMailDomain.validateUsername("azerty@ac-test") ); // email interdit
        assertNull( CheckMailDomain.validateUsername("azerty") ); // ok
        assertNull( CheckMailDomain.validateUsername("azer_ty") ); // ok
        assertNull( CheckMailDomain.validateUsername("azer-ty") ); // ok
        assertNotNull( CheckMailDomain.validateUsername("azer.ty") ); //. interdit
        
    }
}
