/*
 * Keycloak plugin Mail Domain Check
 * 
 * Copyright © 2014-2023 Pôle de Compétence Logiciels Libres EOLE <eole@ac-dijon.fr>
 * 
 * LICENCE PUBLIQUE DE L'UNION EUROPÉENNE v. 1.2 :
 * in french: https://joinup.ec.europa.eu/sites/default/files/inline-files/EUPL%20v1_2%20FR.txt
 * in english https://joinup.ec.europa.eu/sites/default/files/custom-page/attachment/2020-03/EUPL-1.2%20EN.txt
 */
package org.eole.keycloak ;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class RegistrationProfileWithMailDomainCheckTest
{

     public boolean validate(String username, String email, String validDomains)
     {
        String eventError = CheckMailDomain.validateUsername(username);
        if( eventError != null )
        {
            return false;
        }
        
        if( email == null )
        {
           return false;
        }
        
        if( CheckMailDomain.validateEmail(email, validDomains ) == null )
        {
            return false;
        }

        return true;
     }



    @Test
    public void test01()
    {
        StringBuffer whiteDomains = new StringBuffer(1000);
        whiteDomains.append("ac-dijon.fr\n");
        whiteDomains.append("education.gouv.fr\n");
        whiteDomains.append("ac-toulouse.fr\n");
        whiteDomains.append("ac-guyane.fr\n");
        whiteDomains.append("ac-versailles.fr\n");
        whiteDomains.append("ac-reims.fr\n");
        whiteDomains.append("ac-nancy-metz.fr\n");
        whiteDomains.append("ac-paris.fr\n");
        whiteDomains.append("ac-creteil.fr\n");
        whiteDomains.append("ac-strasbourg.fr\n");
        whiteDomains.append("ac-lyon.fr\n");
        whiteDomains.append("igesr.gouv.fr\n");
        whiteDomains.append("hceres.gouv.fr\n");
        whiteDomains.append("recherche.gouv.fr\n");
        whiteDomains.append("enseignementsup.gouv.fr\n");
        whiteDomains.append("ac-grenoble.fr\n");
        whiteDomains.append("reseau-canope.fr\n");
        whiteDomains.append("ac-cned.fr\n");
        whiteDomains.append("clemi.fr\n");
        whiteDomains.append("ac-nice.fr\n");
        whiteDomains.append("ac-besancon.fr\n");
        whiteDomains.append("ac-rennes.fr\n");
        whiteDomains.append("ac-nantes.fr\n");
        whiteDomains.append("ac-orleans-tours.fr\n");
        whiteDomains.append("ac-montpellier.fr\n");
        whiteDomains.append("ac-toulouse.fr\n");
        whiteDomains.append("ac-bordeaux.fr\n");
        whiteDomains.append("ac-poitiers.fr\n");
        whiteDomains.append("ac-limoges.fr\n");
        whiteDomains.append("ac-spm.fr\n");
        whiteDomains.append("ac-lille.fr\n");
        whiteDomains.append("ac-amiens.fr\n");
        whiteDomains.append("ac-normandie.fr\n");
        whiteDomains.append("ac-rouen.fr\n");
        whiteDomains.append("ac-caen.fr\n");
        whiteDomains.append("ac-noumea.nc\n");
        whiteDomains.append("loyalty.nc\n");
        whiteDomains.append("province-iles.nc\n");
        whiteDomains.append("province-nord.nc\n");
        whiteDomains.append("province-sud.nc\n");
        whiteDomains.append("ac-polynesie.pf\n");
        whiteDomains.append("ac-wf.wf\n");
        whiteDomains.append("ac-corse.fr\n");
        whiteDomains.append("ac-guadeloupe.fr\n");
        whiteDomains.append("ac-reunion.fr\n");
        whiteDomains.append("ac-martinique.fr\n");
        whiteDomains.append("ac-mayotte.fr\n");
        whiteDomains.append("ac-aix-marseille.fr\n");
        whiteDomains.append("region-academique-aura.fr\n");
        whiteDomains.append("region-academique-auvergne-rhone-alpes.fr\n");
        whiteDomains.append("region-academique-bfc.fr\n");
        whiteDomains.append("region-academique-bourgogne-franche-comte.fr\n");
        whiteDomains.append("region-academique-bretagne.fr\n");
        whiteDomains.append("region-academique-centre-val-de-loire.fr\n");
        whiteDomains.append("region-academique-corse.fr\n");
        whiteDomains.append("region-academique-grand-est.fr\n");
        whiteDomains.append("region-academique-guadeloupe.fr\n");
        whiteDomains.append("region-academique-guyane.fr\n");
        whiteDomains.append("region-academique-hauts-de-france.fr\n");
        whiteDomains.append("region-academique-idf.fr\n");
        whiteDomains.append("region-academique-ile-de-france.fr\n");
        whiteDomains.append("region-academique-martinique.fr\n");
        whiteDomains.append("region-academique-normandie.fr\n");
        whiteDomains.append("region-academique-mayotte.fr\n");
        whiteDomains.append("region-academique-nouvelle-aquitaine.fr\n");
        whiteDomains.append("region-academique-occitanie.fr\n");
        whiteDomains.append("region-academique-paca.fr\n");
        whiteDomains.append("region-academique-provence-alpes-cote-dazur.fr\n");
        whiteDomains.append("region-academique-pays-de-la-loire.fr\n");
        whiteDomains.append("region-academique-reunion.fr\n");
        whiteDomains.append("renater.fr\n");
        whiteDomains.append("hceres.fr\n");
        whiteDomains.append("reseau-canope.fr\n");
        whiteDomains.append("cned.fr\n");
        whiteDomains.append("ac-cned.fr\n");
        whiteDomains.append("sports.gouv.fr\n");
        whiteDomains.append("jeunesse-sports.gouv.fr\n");
        whiteDomains.append("service-civique.gouv.fr\n");
        whiteDomains.append("diges.gouv.fr\n");
        whiteDomains.append("onisep.fr\n");
        whiteDomains.append("ac-clermont.fr\n");
        whiteDomains.append("pix.fr\n");
        whiteDomains.append("mlfmonde.org\n");
        whiteDomains.append("*aefe.fr\n");
        whiteDomains.append("educagri.fr\n");
        whiteDomains.append(".fr\n");
        whiteDomains.append("region-academique-*.fr\n");
        whiteDomains.append("\n");
        
        assertTrue( validate("test", "test@aefe.fr", whiteDomains.toString() )); // okdomain
        assertFalse( validate("test1", "test.aefe.fr@gmail.com", whiteDomains.toString() )); // bad domain gmail
        assertFalse( validate("toto", "test@toto.fr", whiteDomains.toString() )); // bad domain toto
        assertFalse( validate("toto", "test@", whiteDomains.toString() )); // bad domain
        assertTrue( validate("raf", "ratest@region-academique-test.fr", whiteDomains.toString() )); // ok 3 car
        assertFalse( validate("ra", "ratest@region-academique-test.fr", whiteDomains.toString() )); // nok 3 car
    }

    @Test
    public void test02()
    {
        StringBuffer whiteDomains = new StringBuffer(1000);
        whiteDomains.append("region-??ademique-*.fr\n");
        
        assertTrue( validate("rafc", "ratest@region-academique-test.fr", whiteDomains.toString() ));
    }
    
    @Test
    public void test03()
    {
        StringBuffer whiteDomains = new StringBuffer(1000);
        whiteDomains.append("region-??ademique-*.fr\n");
        
        assertFalse( validate(" rafc", "ratest@region-academique-test.fr", whiteDomains.toString() ));
    }

    @Test
    public void test04()
    {
        StringBuffer whiteDomains = new StringBuffer(1000);
        whiteDomains.append("region-??ademique-*.fr\n");
        
        assertTrue( validate("parenthese", "ratest@region-academique-test.fr", whiteDomains.toString() ));
        assertFalse( validate(" (parenthese", "ratest@region-academique-test.fr", whiteDomains.toString() ));
    }
    
}
