/*
 * Keycloak plugin Mail Domain Check
 *
 * Copyright © 2014-2023 Pôle de Compétence Logiciels Libres EOLE <eole@ac-dijon.fr>
 *
 * LICENCE PUBLIQUE DE L'UNION EUROPÉENNE v. 1.2 :
 * in french: https://joinup.ec.europa.eu/sites/default/files/inline-files/EUPL%20v1_2%20FR.txt
 * in english https://joinup.ec.europa.eu/sites/default/files/custom-page/attachment/2020-03/EUPL-1.2%20EN.txt
 */
package org.eole.keycloak;

import java.util.List;
import java.util.regex.Pattern;

/**
 * The Class CheckMailDomain.
 */
public class CheckMailDomain
{

    /** The Constant ALLOWED_USERNAME_PATTERN.
     * accept : alphanumeric minuscules et majuscules, chiffres, le tiret, et le souligné
     * */
    private static final Pattern ALLOWED_USERNAME_PATTERN = Pattern.compile("^[0-9a-zA-Z\\-_]+$");

    /**
     * Globmatches.
     *
     * @param text
     *            the text
     * @param glob
     *            the glob
     * @param level
     *            the level
     * @return true, if successful
     */
    private static final boolean globmatches(String text, String glob, int level)
    {

        if (text == null | glob == null | text.length() > 200)
        {
            return false;
        }

        String rest = null;
        int pos = glob.indexOf('*');
        if (pos != -1)
        {
            rest = glob.substring(pos + 1);
            glob = glob.substring(0, pos);
        }

        if (glob.length() > text.length())
        {
            // System.out.println("level="+level+" text="+text+" glob="+glob+" rest="+rest + " : len glob > len text -> false");
            return false;
        }

        // handle the part up to the first *
        for (int i = 0; i < glob.length(); i++)
        {
            char c = glob.charAt(i);
            if (c != '?')
            {
                String globPart = glob.substring(i, i + 1);
                String textPart = text.substring(i, i + 1);

                if (!globPart.equalsIgnoreCase(textPart))
                {
                    // System.out.println("level="+level+" text="+text+" glob="+glob+" rest="+rest + " : glob part != text part -> false");
                    return false;
                }
            } else
            {
                // System.out.println("level="+level+" text="+text+" glob="+glob+" rest="+rest + " : ignore c="+ c);
            }
        }

        // System.out.println("text="+text+" glob="+glob+" rest="+rest +" : match continue");

        // recurse for the part after the first *, if any
        if (rest == null)
        {
            if (glob.length() == text.length())
            {
                // System.out.println("level="+level+" text="+text+" glob="+glob+" rest="+rest + " : rest null, len glob = len text -> true");
                return true;
            } else
            {
                // System.out.println("level="+level+" text="+text+" glob="+glob+" rest="+rest + " : rest null, len glob != len text -> false");
                return false;
            }
        } else
        {
            for (int i = glob.length(); i <= text.length(); i++)
            {
                String sub = text.substring(i);
                if (globmatches(sub, rest, level + 1))
                {
                    // System.out.println("level="+level+" text="+text+" glob="+glob+" MATCHES !");
                    return true;
                }
            }
            // System.out.println("level="+level+" text="+text+" glob="+glob+" -> false");
            return false;
        }
    }

    /**
     * Validate username.
     *
     * @param username
     *            the username
     * @return Null si OK, else the error message
     */
    public static final String validateUsername(String username)
    {
        if (username == null || username.length() == 0)
        {
            return "L'identifiant d'utilisateur ne doit pas être vide";
        }
        if (username.startsWith(" "))
        {
            return "L'identifiant d'utilisateur ne doit pas commencer par un espace";
        }
        if (username.indexOf("@") > 0)
        {
            return "Pas de '@' dans l'identifiant d'utilisateur (pas d'email ici)";
        }
        if (username.indexOf(" ") > 0)
        {
            return "Pas de ' ' dans l'identifiant d'utilisateur";
        }
        if (username.length() > 45)
        {
            return "L'identifiant d'utilisateur ne doit pas contenir plus de 45 caractères";
        }
        if (username.length() < 3)
        {
            return "L'identifiant d'utilisateur doit contenir au moins 3 caractères";
        }
        if (ALLOWED_USERNAME_PATTERN.matcher(username).matches() == false)
        {
            return "Caractère(s) interdit(s) dans l'identifiant d'utilisateur";
        }
        return null; // pas d'erreur
    }

    /**
     * Validate email.
     *
     * @param email
     *            the email
     * @param validDomains
     *            the valid domains
     * @return true, if successful
     */
    public static final String validateEmail(String email, String validDomains)
    {
        if (email == null)
        {
            return null;
        }
        if (validDomains == null )
        {
            // laisse tout passer..
            return "Ok pour *";
        }
        String[] domains = validDomains.split("\n");
        if (domains.length == 0)
        {
            // laisse tout passer..
            return "Ok pour *";
        }
        for (String domain : domains)
        {
            if (email.endsWith("@" + domain))
            {
                return "Ok pour @" + domain ;
            } else if (globmatches(email, "*@" + domain, 0))
            {
                return "Ok pour *@" + domain ;
            }
        }
        return null;
    }

    /**
     * Validate email.
     *
     * @param email
     *            the email
     * @param validDomains
     *            the valid domains
     * @return true, if successful
     */
    public static final String validateEmail(String email, List<String> validDomains)
    {
        if (email == null)
        {
            return null;
        }
        if (validDomains == null || validDomains.size() == 0)
        {
            // laisse tout passer..
            return "Ok pour *";
        }
        for (String domain : validDomains)
        {
            if (email.endsWith("@" + domain))
            {
                return "Ok pour @" + domain ;
            } else if (globmatches(email, "*@" + domain, 0))
            {
                return "Ok pour *@" + domain ;
            }
        }
        return null;
    }

}
