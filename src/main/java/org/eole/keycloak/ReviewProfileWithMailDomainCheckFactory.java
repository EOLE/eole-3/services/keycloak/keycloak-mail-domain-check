/*
 * Keycloak plugin Mail Domain Check
 * 
 * Copyright © 2014-2023 Pôle de Compétence Logiciels Libres EOLE <eole@ac-dijon.fr>
 * 
 * LICENCE PUBLIQUE DE L'UNION EUROPÉENNE v. 1.2 :
 * in french: https://joinup.ec.europa.eu/sites/default/files/inline-files/EUPL%20v1_2%20FR.txt
 * in english https://joinup.ec.europa.eu/sites/default/files/custom-page/attachment/2020-03/EUPL-1.2%20EN.txt
 */
package org.eole.keycloak;

import org.jboss.logging.Logger;
import org.keycloak.Config;
import org.keycloak.authentication.Authenticator;
import org.keycloak.authentication.AuthenticatorFactory;
import org.keycloak.models.AuthenticationExecutionModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;
import org.keycloak.provider.ProviderConfigProperty;
import org.keycloak.representations.idm.IdentityProviderRepresentation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * A factory for creating ReviewProfileWithMailDomainCheck objects.
 *
 * @author <a href="mailto:mposolda@redhat.com">Marek Posolda</a>
 */
public class ReviewProfileWithMailDomainCheckFactory implements AuthenticatorFactory {

    /** The Constant logger. */
    private static final Logger logger = Logger.getLogger(ReviewProfileWithMailDomainCheckFactory.class);

    /** The Constant PROVIDER_ID. */
    public static final String PROVIDER_ID = "idp-review-profile-with-domain-check";
    
    /** The singleton. */
    static ReviewProfileWithMailDomainCheck SINGLETON = new ReviewProfileWithMailDomainCheck();

    /** The Constant UPDATE_PROFILE_ON_FIRST_LOGIN. */
    public static final String UPDATE_PROFILE_ON_FIRST_LOGIN = "update.profile.on.first.login";

    /**
     * Creates the.
     *
     * @param session
     *            the session
     * @return the authenticator
     */
    @Override
    public Authenticator create(KeycloakSession session) {
        return SINGLETON;
    }

    /**
     * Inits the.
     *
     * @param config
     *            the config
     */
    @Override
    public void init(Config.Scope config) {
        logger.infof("Load ReviewProfileWithMailDomainCheckFactory");
    }

    /**
     * Post init.
     *
     * @param factory
     *            the factory
     */
    @Override
    public void postInit(KeycloakSessionFactory factory) {

    }

    /**
     * Close.
     */
    @Override
    public void close() {

    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    @Override
    public String getId() {
        return PROVIDER_ID;
    }

    /**
     * Gets the reference category.
     *
     * @return the reference category
     */
    @Override
    public String getReferenceCategory() {
        return "reviewProfile";
    }

    /**
     * Checks if is configurable.
     *
     * @return true, if is configurable
     */
    @Override
    public boolean isConfigurable() {
        return true;
    }

    /**
     * Gets the requirement choices.
     *
     * @return the requirement choices
     */
    @Override
    public AuthenticationExecutionModel.Requirement[] getRequirementChoices() {
        return REQUIREMENT_CHOICES;
    }

    /**
     * Gets the display type.
     *
     * @return the display type
     */
    @Override
    public String getDisplayType() {
        return "Review Profile With username check (Apps)";
    }

    /**
     * Gets the help text.
     *
     * @return the help text
     */
    @Override
    public String getHelpText() {
        return "User reviews and updates profile data retrieved from Identity Provider in the displayed form, and check username validity";
    }

    /**
     * Checks if is user setup allowed.
     *
     * @return true, if is user setup allowed
     */
    @Override
    public boolean isUserSetupAllowed() {
        return false;
    }

    /** The Constant configProperties. */
    private static final List<ProviderConfigProperty> configProperties = new ArrayList<ProviderConfigProperty>();

    static {
        ProviderConfigProperty property;
        property = new ProviderConfigProperty();
        property.setName(UPDATE_PROFILE_ON_FIRST_LOGIN);
        property.setLabel("Update Profile on First Login");
        property.setType(ProviderConfigProperty.LIST_TYPE);
        List<String> updateProfileValues = Arrays.asList(
                        IdentityProviderRepresentation.UPFLM_ON,
                        IdentityProviderRepresentation.UPFLM_MISSING,
                        IdentityProviderRepresentation.UPFLM_OFF);
        property.setOptions(updateProfileValues);
        property.setDefaultValue(IdentityProviderRepresentation.UPFLM_MISSING);
        property.setHelpText("Define conditions under which a user has to review and update his profile after first-time login. Value 'On' means that"
                + " page for reviewing profile will be displayed and user can review and update his profile. Value 'off' means that page won't be displayed."
                + " Value 'missing' means that page is displayed just when some required attribute is missing (wasn't downloaded from identity provider). Value 'missing' is the default one."
                + " WARN: In case that user clicks 'Review profile info' on link duplications page, the update page will be always displayed. You would need to disable this authenticator to never display the page.");

        configProperties.add(property);
    }


    /**
     * Gets the config properties.
     *
     * @return the config properties
     */
    @Override
    public List<ProviderConfigProperty> getConfigProperties() {
        return configProperties;
    }
}
