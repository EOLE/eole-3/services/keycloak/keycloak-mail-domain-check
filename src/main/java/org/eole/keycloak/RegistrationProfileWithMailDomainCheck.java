/*
 * Keycloak plugin Mail Domain Check
 * 
 * Copyright © 2014-2023 Pôle de Compétence Logiciels Libres EOLE <eole@ac-dijon.fr>
 * 
 * LICENCE PUBLIQUE DE L'UNION EUROPÉENNE v. 1.2 :
 * in french: https://joinup.ec.europa.eu/sites/default/files/inline-files/EUPL%20v1_2%20FR.txt
 * in english https://joinup.ec.europa.eu/sites/default/files/custom-page/attachment/2020-03/EUPL-1.2%20EN.txt
 */
package org.eole.keycloak;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.jboss.logging.Logger;
import org.keycloak.authentication.FormContext;
import org.keycloak.authentication.ValidationContext;
import org.keycloak.authentication.forms.RegistrationPage;
import org.keycloak.authentication.forms.RegistrationUserCreation;
import org.keycloak.events.Details;
import org.keycloak.events.Errors;
import org.keycloak.forms.login.LoginFormsProvider;
import org.keycloak.models.AuthenticatorConfigModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.UserModel;
import org.keycloak.models.utils.FormMessage;
import org.keycloak.provider.ProviderConfigProperty;
import org.keycloak.services.messages.Messages;
import org.keycloak.userprofile.UserProfile;
import org.keycloak.userprofile.UserProfileContext;
import org.keycloak.userprofile.UserProfileProvider;

import jakarta.ws.rs.core.MultivaluedMap;

/**
 * The Class RegistrationProfileWithMailDomainCheck.
 */
public class RegistrationProfileWithMailDomainCheck extends RegistrationUserCreation 
{
    /** The Constant logger. */
    private static final Logger logger = Logger.getLogger(RegistrationProfileWithMailDomainCheck.class);

   /** The Constant PROVIDER_ID. */
   public static final String PROVIDER_ID = "registration-mail-check-action";
   
   /** The Constant PROVIDER_ID. */
   public static final String VALID_DOMAINS_ATTRIBUT = "validDomains";

   /**
    * Gets the display type.
    *
    * @return the display type
    */
   @Override
    public String getDisplayType() {
        return "Registration User Creation with email domain check (Apps)";
   }


   /**
    * Gets the id.
    *
    * @return the id
    */
   @Override
   public String getId() {
      return PROVIDER_ID;
   }

   /**
    * Checks if is configurable.
    *
    * @return true, if is configurable
    */
   @Override
    public boolean isConfigurable() {
        return true;
   }


   /**
    * Gets the help text.
    *
    * @return the help text
    */
   @Override
   public String getHelpText() {
      return "Adds validation of domain emails for registration";
   }

   /** The Constant CONFIG_PROPERTIES. */
   private static final List<ProviderConfigProperty> CONFIG_PROPERTIES = new ArrayList<ProviderConfigProperty>();

   static {
      ProviderConfigProperty property;
      property = new ProviderConfigProperty();
      property.setName(VALID_DOMAINS_ATTRIBUT);
      property.setLabel("Valid domain for emails");
      property.setType(ProviderConfigProperty.TEXT_TYPE);
      property.setHelpText("List mail domains authorized to register");
      CONFIG_PROPERTIES.add(property);
   }

   
  

   /**
    * Gets the config properties.
    *
    * @return the config properties
    */
   @Override
   public List<ProviderConfigProperty> getConfigProperties() {
      return CONFIG_PROPERTIES;
   }

   /**
    * Validate.
    *
    * @param context
    *            the context
    */
   @Override
   public void validate(ValidationContext context) {
      MultivaluedMap<String, String> formData = context.getHttpRequest().getDecodedFormParameters();

      String email = formData.getFirst(UserModel.EMAIL);
      String username = formData.getFirst(UserModel.USERNAME);

      List<FormMessage> errors = new ArrayList<>();
      logger.debugf("RegistrationProfileWithMailDomainCheck.validate1 -> username '%s' email='%s'.", username, email);
      
      String msgErreur = CheckMailDomain.validateUsername(username);
      if( msgErreur != null )
      {
          logger.debugf("RegistrationProfileWithMailDomainCheck.validate2 -> Erreur username '%s'.", msgErreur);
          context.getEvent().detail(Details.USERNAME, username);
          errors.add(new FormMessage(RegistrationPage.FIELD_USERNAME, msgErreur));
          context.error(Errors.INVALID_REGISTRATION);
          context.validationError(formData, errors);
          return;
      }

      if(email == null)
      {
          logger.debugf("RegistrationProfileWithMailDomainCheck.validate3-> Erreur mail '%s'.", email);
          context.getEvent().detail(Details.EMAIL, email);
          errors.add(new FormMessage(RegistrationPage.FIELD_EMAIL, Messages.INVALID_EMAIL));
          context.error(Errors.INVALID_REGISTRATION);
          context.validationError(formData, errors);
          return;
      }

      AuthenticatorConfigModel mailDomainConfig = context.getAuthenticatorConfig();
      String validDomains = mailDomainConfig.getConfig().getOrDefault(VALID_DOMAINS_ATTRIBUT,"exemple.org");
      
      logger.debugf("RegistrationProfileWithMailDomainCheck.validate4 -> validDomains '%s'.", validDomains);
      String msgOk = CheckMailDomain.validateEmail(email, validDomains);
      if ( msgOk == null )
      {
          logger.debugf("RegistrationProfileWithMailDomainCheck.validate5 -> Erreur domain '%s'.", email);
          context.getEvent().detail(Details.EMAIL, email);
          errors.add(new FormMessage(RegistrationPage.FIELD_EMAIL, Messages.INVALID_EMAIL));
          context.error(Errors.INVALID_REGISTRATION);
          context.validationError(formData, errors);
          return;
      } 
      
      logger.debugf("RegistrationProfileWithMailDomainCheck.validate6 -> cas général, go super.validate.");
      super.validate(context);
   }


   /**
    * Builds the page.
    *
    * @param context
    *            the context
    * @param form
    *            the form
    */
   @Override
   public void buildPage(FormContext context, LoginFormsProvider form) {

      String validDomains = context.getAuthenticatorConfig().getConfig().getOrDefault("validDomains","exemple.org");
      logger.debugf("RegistrationProfileWithMailDomainCheck.buildPage -> validDomains '%s'.", validDomains);
      form.setAttribute(VALID_DOMAINS_ATTRIBUT, validDomains);
   }

}