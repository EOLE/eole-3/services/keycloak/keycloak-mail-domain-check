/*
 * Keycloak plugin Mail Domain Check
 * 
 * Copyright © 2014-2023 Pôle de Compétence Logiciels Libres EOLE <eole@ac-dijon.fr>
 * 
 * LICENCE PUBLIQUE DE L'UNION EUROPÉENNE v. 1.2 :
 * in french: https://joinup.ec.europa.eu/sites/default/files/inline-files/EUPL%20v1_2%20FR.txt
 * in english https://joinup.ec.europa.eu/sites/default/files/custom-page/attachment/2020-03/EUPL-1.2%20EN.txt
 */
package org.eole.keycloak;

import java.util.ArrayList;
import java.util.List;

import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;

import org.jboss.logging.Logger;
import org.keycloak.authentication.AuthenticationFlowContext;
import org.keycloak.authentication.authenticators.broker.IdpReviewProfileAuthenticator;
import org.keycloak.authentication.authenticators.broker.util.SerializedBrokeredIdentityContext;
import org.keycloak.authentication.forms.RegistrationPage;
import org.keycloak.broker.provider.BrokeredIdentityContext;
import org.keycloak.forms.login.LoginFormsProvider;
import org.keycloak.models.utils.FormMessage;
import org.keycloak.services.validation.Validation;

/**
 * The Class ReviewProfileWithMailDomainCheck.
 *
 * @author <a href="mailto:gilles.grandgerard@ac-dijon.fr">Gilles GRANDGERARD</a>
 * @version $Revision: 1 $
 */
public class ReviewProfileWithMailDomainCheck extends IdpReviewProfileAuthenticator {

    /** The Constant logger. */
    private static final Logger logger = Logger.getLogger(ReviewProfileWithMailDomainCheck.class);

    /**
     * Requires update profile page.
     *
     * @param context
     *            the context
     * @param userCtx
     *            the user ctx
     * @param brokerContext
     *            the broker context
     * @return true, if successful
     */
    protected boolean requiresUpdateProfilePage(AuthenticationFlowContext context, SerializedBrokeredIdentityContext userCtx, BrokeredIdentityContext brokerContext) {
        String usernameIdp = userCtx.getUsername();
        logger.debugf("requiresUpdateProfilePage -> usernameIdp '%s'.", usernameIdp);
        
        String msgErreur = CheckMailDomain.validateUsername(usernameIdp);
        if( msgErreur != null )
        {
            logger.debugf("requiresUpdateProfilePage -> non car %s", msgErreur);
            return true;
        }
        else
        {
            return super.requiresUpdateProfilePage(context, userCtx, brokerContext);
        }
    }

  /**
   * Action impl.
   *
   * @param context
   *            the context
   * @param userCtx
   *            the user ctx
   * @param brokerContext
   *            the broker context
   */
  @Override
  protected void actionImpl(AuthenticationFlowContext context, SerializedBrokeredIdentityContext userCtx, BrokeredIdentityContext brokerContext) 
  {
      String usernameIdp = userCtx.getUsername();
      logger.debugf("actionImpl -> usernameIdp='%s'.", usernameIdp);
      MultivaluedMap<String, String> formData = context.getHttpRequest().getDecodedFormParameters();
      //logger.debugf("actionImpl -> formData %s ", formData ); attention au mot de passe ! 
      String username = formData.getFirst(Validation.FIELD_USERNAME);
      logger.debugf("actionImpl -> username %s ", username ); 

      String email = formData.getFirst(Validation.FIELD_EMAIL);
      logger.debugf("actionImpl -> email %s ", email); 

      String msgErreur = CheckMailDomain.validateUsername(username);
      if( msgErreur != null )
      {
          logger.debugf("actionImpl -> reject car %s", msgErreur);
          
          List<FormMessage> errors = new ArrayList<>();
          errors.add(new FormMessage(RegistrationPage.FIELD_USERNAME, msgErreur));
          
          Response challenge = context.form()
            .setErrors(errors)
            .setAttribute(LoginFormsProvider.UPDATE_PROFILE_CONTEXT_ATTR, userCtx)
            .setFormData(formData)
            .createUpdateProfilePage();
          context.challenge(challenge);
          return;
      }
      else
      {
          super.actionImpl(context, userCtx, brokerContext);
      }
  }
    
}
