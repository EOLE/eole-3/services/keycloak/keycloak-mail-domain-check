FROM hub.eole.education/test/eole3-keycloak:22.0.5-eole3.1
RUN rm opt/keycloak/standalone/deployments/keycloak-mail-whitelisting-*.jar || true
RUN rm opt/keycloak/standalone/deployments/keycloak-mail-domain-check-*.jar || true
COPY target/keycloak-mail-domain-check-*.jar opt/keycloak/standalone/deployments/
COPY standalone.xml opt/keycloak/standalone/configuration/
RUN rm opt/keycloak/standalone/deployments/keycloak-mail-domain-check*javadoc.jar || true
RUN rm opt/keycloak/standalone/deployments/keycloak-mail-domain-check*sources.jar || true
ENTRYPOINT ["/opt/keycloak/bin/kc.sh" ]
