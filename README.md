### Keycloak - Whitelist email domain for registration

This extension allows you to validate email domain used for registration in keycloak to accept only a finite list of domain.

You can use basic [glob syntax](https://en.wikipedia.org/wiki/Glob_(programming))
(only `*` and `?` are supported)

## How to install

1. Stop Keycloak.

2. Copy the jar in $KEYCLOAK_HOME/providers/.

4. Rebuil Keycloak

    kc.sh build

3. Start Keycloak.

    kc.sh start

## How to use


### 1) Configure Realm Authentication Registration

1. Login to admin console.  Hit browser refresh if you are already logged in so that the new providers show up.

2. Go to the **Realm Settings**  menu item and go to the **Login** tab, Configure the realm to accept '**User registration**' and '**verify email**' (this is important!), and '**Save**'

3. Go to the **Authentication** menu item and go to the **Flows** tab, you will be able to view the currently
   defined flows.  You cannot modify an built in flows, so, to add the Authenticator you
   have to copy an existing flow or create your own.  
   
4. Click on "**Registration**" flow.  On the détail view, on **Action** button select '**Duplicate**'. In the modal form, set new name to "**Registration With Email Domain Check**" and click on **Duplicate** button

5.  (Warning, Don't use "Add step" button  nest to 'Add sub-flow' ! )
    On the first line : "**Registration With Email Domain Check Registration Form**", Click on **+**,and select '**add step**'. 
    A form open. 

6. Choose provider "**Registration User Creation With Email Domain Check (apps)**", and **Add**

7. Drag and drop "**Registration User Creation With Email Domain Check (apps)**" in first step of the sub-tree (important)

8. delete "**Registration User Creation**". The "**Registration User Creation With Email Domain Check (apps)**" inherit from Registration User Creation, must not be use together.

9. first, set **REQUIRED** for execution "**Registration User Creation With Email Domain Check (apps)**" 

10. this new execution need to be configured otherwise, keycloak will only accept "exemple.org" domains

11. click on **Settings** (gear wheel) on the line of "**Registration User Creation With Email Domain Check (apps)**", and select '**config**"

12. Set alias to '**ACA**', and Valid domains for emails to '**ac-dijon.fr**' and '**ac-besancon.fr**' on the second line, and '**Save**'

13. On the **Action** Button of the Flow, choose **Bind flow**. In the A popup, Change the 'binding type' to "**Registration flow**" and '**Save**'

14. return to the **Authentication** menu and,
    check that **Registration With Email Domain Check Registration Form** is "Used By" registration flow (green ball)
    check that **Registration** is "Not in use"

15. Ready to test "registration" !

### 2) Test Realm Authentication Registration Check

1. On another browser, go to http://localhost:8080/auth/realms/master/account/?referrer=security-admin-console#/

2. On login form, click on link "**Register**" 

3. On register form, define all values... but try email = "**test@gmail.com**"

4. Click on "**Register**"

5. A red message must is "**Invalid email address.**"

6. Change email = "**test@ac-dijon.fr**", and re hit password

7. Click on "**Register**"

8. The account is accepted, and created. So the account console is displayed

8. Ready to test "registration" !

### 3) Configure Realm Authentication Broker

1. Login to admin console.  Hit browser refresh if you are already logged in so that the new providers show up.

2. Go to the **Authentication** menu item and go to the **Flows** tab, you will be able to view the currently
   defined flows.  You cannot modify an built in flows, so, to add the Authenticator you
   have to copy an existing flow or create your own.

3. Click "**First Broker Login**" flow on the grid. On the détail view, on **Action** Button select '**Duplicate**', and set new name to "**First Broker Login With Check Email Domain**" and click on **Duplicate** button

4. In your copy, click the **Add Step** button. Select "**Review Profile With Username Check (apps)**" (page 3!) and click on "**Add**". 

5. To take the new action to be up on first place. 
    Click on **>** belong to "**User creation or linking**" to colapse tree
    Drag and drop "**Review Profile With Username Check (apps)**" before "**Review Profile (review profile config)**"
   
6. To suppress the old **Review Profile (review profile config)** action, Click on bin icone. 
   (tips: in the code ReviewProfileWithMailDomainCheck extends IdpReviewProfileAuthenticator !)

7. click on **Settings** (gear wheel) on the line of "**Registration User Creation With Email Domain Check (apps)**", and select '**config**"

8. Set alias to '**ACA**', and Valid domains for emails to '**ac-dijon.fr**' and '**ac-besancon.fr**' on the second line, and '**Save**'

7. On the Action Button, choose **Bind flow**. In the A popup, Change the 'binding type' to "**Registration flow**" and '**Save**'

8. return to the **Authentication** menu and,
    check that **First Broker Login With Check Email Domain** is "Used By" Browserflow (green ball)
    check that **browser** is "Not in use"

9. Ready to test Idp Login

10. You must create an Idp before test "Review Profile With Username Check " ! 
   
   You need 2 idp : **Target Idp** and **Local Keycloak**
   
   **You need an account on Target Idp with an issue !!**

#### On Target Idp

First, you must create an **client** on **Target Idp**

1. Login to admin console **Target Idp**.

2. Go to the **Clients** menu item and click on **create** button. Set ClientId to "test-idp", Client Protocol to "**openid-connect**". **Save**

3. Open client "test-idp", Set "Access Type" to "**confidential**", Valide Redirect URL to "*****" (Only for test environment!!). **Save**

4. Click on tab "**Credentials**". In "Client Authenticator", Select "**Client id and secret**". And save secret to next step.

#### On Local Keycloak

Second, you must create an **Identity Providers** to authentify on **Target Idp**

1. Login to admin console.  Hit browser refresh if you are already logged in so that the new providers show up.

2. Go to the **Identity Providers** menu item and add provider **Keycloak OpenId Connect**.

2. Go to the **Authentication** menu item and go to the **Flows** tab, you will be able to view the currently
   defined flows.  You cannot modify an built in flows, so, to add the Authenticator you
   have to copy an existing flow or create your own.

3. Set "**Display Name**" (ex. MIM)

4. Set "**Autorization URL**", (ex.: <serveur>/auth/realms/<realm>/protocol/openid-connect/auth )

5. Set "**Token URL**", (ex.: <serveur>/auth/realms/<realm>/protocol/openid-connect/token )

5. Set "**Logout URL**", (ex.: <serveur>/auth/realms/<realm>/protocol/openid-connect/logout )

6. Set "**Client Authentification**" = "**Client secret as post**"

7. Set "**Client ID**" = "test-idp"

8. Set "**Client Secret**" = the secret value (step 4 on Target Idp)

9. Selectionner dans "**First Login Flow**" = "First Broker Login With Check Email Domain" (créer précédement)

10. Click **Save**

#### How to test 

1. In an another browser or private windows, go to http://localhost:8080/auth/realms/master/account/?referrer=security-admin-console

2. On login screen, the idp button must be present. Click on idp button.

3. You are redirect to Target Idp. Enter username/password and login.

4. On first login, the keycloack display "Update Account Information" with "Username", "Email", "First name" and "Last name"

5. Change username to check the rules. Hit **Submit** and the error are display below the field.

6. If no error, the authentification continue. Then, the user is created in keycloak DB. (If you want test again, you need to delete the user !)

## How to build and deploy the plugin

1. Configure Logger Keycloak Server 

```
    <profile>
        <subsystem xmlns="urn:jboss:domain:logging:8.0">
            ...
            <logger category="org.keycloak.authentication.authenticators.broker">
                <level name="DEBUG"/>
            </logger>
            <logger category="org.eole">
                <level name="DEBUG"/>
            </logger>
            ...
```

2. First, start Keycloak in debug mode. See [Getting Started](https://github.com/keycloak/keycloak#getting-started) 

```
$KEYCLOAK_HOME/bin/standalone.sh -debug
```

3. Execute the follow.  This will build the plugin and deploy it

```
./mvnw clean install wildfly:deploy
```

4. Access the application

The application will be running at the following URL: <http://localhost:8080/>.


5. Undeploy the Archive

```
./mvnw -Pwildfly wildfly:undeploy
```


Debug the Application
------------------------------------

If you want to debug the source code or look at the Javadocs of any library in the project, run either of the following commands to pull them into your local repository. The IDE should then detect them.

    ./mvnw dependency:sources
    ./mvnw dependency:resolve -Dclassifier=javadoc
