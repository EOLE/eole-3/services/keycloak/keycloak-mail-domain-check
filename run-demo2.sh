#!/bin/bash -x 
set -e

# shellcheck disable=SC1091
source ./function.sh

waitUrl http://localhost:8080/auth/realms/master/.well-known/openid-configuration 10

initToolsKcAdm

echo "Get Secret IdpTarget"
export KC_SERVER=https://sso.mim-libre.fr
export ADMIN_MASTER_USER=gillesadmin
export PWD_MASTER_USER=xxxxxxxx
export ADMIN_REALM_NAME=master

kcadm.sh config credentials --server "$KC_SERVER" --realm "$ADMIN_REALM_NAME" --user "$ADMIN_MASTER_USER" --password "$PWD_MASTER_USER"

#curl \
#  -d "client_id=account" \
#  -d "client_secret=YOUR_SECRET" \
#  -d "grant_type=client_credentials" \
#  "http://localhost:9080/auth/realms/myrealm/protocol/openid-connect/token"
  
curl --silent \
   --output "$output/session.json" \
   -d "client_id=admin-cli" \
   -d "username=$ADMIN_MASTER_USER" \
   -d "password=$PWD_MASTER_USER" \
   -d "grant_type=password" \
   "$KC_SERVER/auth/realms/$ADMIN_REALM_NAME/protocol/openid-connect/token"


access_token=$(jq ".[\"access_token\"]" <$output/session.json) 
token_type=$(jq ".[\"token_type\"]" <$output/session.json)

curl -v -X GET "$KC_SERVER/auth/realms/$ADMIN_REALM_NAME/broker/your-provider/token" -H "Accept: application/json" -H "Authorization: Bearer $TKN" | jq .

kcadm.sh get clients/test-fede/installation/providers/keycloak-oidc-keycloak-json \
  -r MIM \
  | jq ".[\"auth-server-url\"]=\"$KC_PUBLIC_HOSTNAME_PUBLIC/auth\"" \
  > $output/keycloak.json
CDU="$?"
if [ "$CDU" -ne 0 ]
then
    echo "Unable to get configuration file"
else
    cat $output/keycloak.json
    
    SECRET=$(jq -r .credentials.secret $output/keycloak.json)
    echo "SECRET=$SECRET"
    
    echo "Keycloak successfully configured."
fi

#########################################
# login realm master Admin
#########################################
if ! loginKeycloack
then
   exit 1
fi

#kcadm.sh config credentials --server http://localhost:8080 --realm master --user admin --password admin
set -x
kcadm.sh delete realms/demo || /bin/true
kcadm.sh delete realms/test || /bin/true
kcadm.sh delete realms/wildfly-realm || /bin/true
kcadm.sh delete realms/demorealm || /bin/true

kcadm.sh create realms -s realm=demorealm -s enabled=true -o >/tmp/realm.json

# active France !
kcadm.sh update realms/demorealm -s internationalizationEnabled=true -s 'supportedLocales=[ "en", "fr" ]' -s defaultLocale=fr

# active logs FULL !!!
kcadm.sh update events/config -r demorealm -s adminEventsEnabled=true \
                                           -s adminEventsDetailsEnabled=true \
                                           -s eventsEnabled=true \
                                           -s eventsExpiration=172800 \
                                           -s 'enabledEventTypes=["LOGIN_ERROR","REGISTER_ERROR","LOGOUT_ERROR","CODE_TO_TOKEN_ERROR","CLIENT_LOGIN_ERROR","FEDERATED_IDENTITY_LINK_ERROR","REMOVE_FEDERATED_IDENTITY_ERROR","UPDATE_EMAIL_ERROR","UPDATE_PROFILE_ERROR","UPDATE_PASSWORD_ERROR","UPDATE_TOTP_ERROR","VERIFY_EMAIL_ERROR","REMOVE_TOTP_ERROR","SEND_VERIFY_EMAIL_ERROR","SEND_RESET_PASSWORD_ERROR","SEND_IDENTITY_PROVIDER_LINK_ERROR","RESET_PASSWORD_ERROR","IDENTITY_PROVIDER_FIRST_LOGIN_ERROR","IDENTITY_PROVIDER_POST_LOGIN_ERROR","CUSTOM_REQUIRED_ACTION_ERROR","EXECUTE_ACTIONS_ERROR","CLIENT_REGISTER_ERROR","CLIENT_UPDATE_ERROR","CLIENT_DELETE_ERROR"]' 

kcadm.sh create users -r demorealm -s username=demo-admin -s enabled=true

kcadm.sh set-password -r demorealm --username demo-admin --new-password admin

kcadm.sh create clients -r demorealm -s clientId=demo-manager-client -s publicClient="true"  -s "redirectUris=[\"http://localhost:8080/*\"]" -s enabled=true

kcadm.sh create roles -r demorealm -s name=demo-manager -s 'description=Demo Manager'

kcadm.sh create roles -r demorealm -s name=demo-user -s 'description=Demo regular user with limited set of permissions'

kcadm.sh add-roles -r demorealm --uusername demo-admin --rolename demo-manager

kcadm.sh get serverinfo -r demorealm --fields 'identityProviders(*)'

# normalement vide ici
#kcadm.sh get identity-provider/instances -r demorealm --fields alias,providerId,enabled

# Identity Provider
kcadm.sh create identity-provider/instances -r demorealm -s alias=MIM \
                                                         -s providerId=keycloak-oidc \
                                                         -s enabled=true \
                                                         -s 'config.useJwksUrl="true"' \
                                                         -s config.authorizationUrl=https://sso.mim-libre.fr/auth/realms/demorealm/protocol/openid-connect/auth \
                                                         -s config.tokenUrl=https://sso.mim-libre.fr/auth/realms/demorealm/protocol/openid-connect/token \
                                                         -s config.clientId=mim-oidc-provider \
                                                         -s config.clientSecret=secret

kcadm.sh create identity-provider/instances -r demorealm -s alias=keycloak-oidc \
                                                         -s providerId=keycloak-oidc \
                                                         -s enabled=true \
                                                         -s 'config.useJwksUrl="true"' \
                                                         -s config.authorizationUrl=http://localhost:8180/auth/realms/demorealm/protocol/openid-connect/auth \
                                                         -s config.tokenUrl=http://localhost:8180/auth/realms/demorealm/protocol/openid-connect/token \
                                                         -s config.clientId=demo-oidc-provider \
                                                         -s config.clientSecret=secret

kcadm.sh create identity-provider/instances -r demorealm -s alias=facebook \
                                                         -s providerId=facebook \
                                                         -s enabled=true \
                                                         -s 'config.useJwksUrl="true"' \
                                                         -s config.clientId=FACEBOOK_CLIENT_ID \
                                                         -s config.clientSecret=FACEBOOK_CLIENT_SECRET

kcadm.sh create identity-provider/instances -r demorealm -s alias=google \
                                                         -s providerId=google \
                                                         -s enabled=true  \
                                                         -s 'config.useJwksUrl="true"' \
                                                         -s config.clientId=GOOGLE_CLIENT_ID \
                                                         -s config.clientSecret=GOOGLE_CLIENT_SECRET

kcadm.sh create identity-provider/instances -r demorealm -s alias=twitter \
                                                         -s providerId=twitter \
                                                         -s enabled=true \
                                                         -s 'config.useJwksUrl="true"' \
                                                         -s config.clientId=TWITTER_API_KEY \
                                                         -s config.clientSecret=TWITTER_API_SECRET

kcadm.sh create identity-provider/instances -r demorealm -s alias=github \
                                                         -s providerId=github \
                                                         -s enabled=true \
                                                         -s 'config.useJwksUrl="true"' \
                                                         -s config.clientId=GITHUB_CLIENT_ID \
                                                         -s config.clientSecret=GITHUB_CLIENT_SECRET

kcadm.sh create identity-provider/instances -r demorealm -s alias=linkedin \
                                                         -s providerId=linkedin \
                                                         -s enabled=true \
                                                         -s 'config.useJwksUrl="true"' \
                                                         -s config.clientId=LINKEDIN_CLIENT_ID \
                                                         -s config.clientSecret=LINKEDIN_CLIENT_SECRET

kcadm.sh create identity-provider/instances -r demorealm -s alias=microsoft \
                                                         -s providerId=microsoft \
                                                         -s enabled=true \
                                                         -s 'config.useJwksUrl="true"' \
                                                         -s config.clientId=MICROSOFT_APP_ID \
                                                         -s config.clientSecret=MICROSOFT_PASSWORD

kcadm.sh create identity-provider/instances -r demorealm -s alias=stackoverflow \
                                                         -s providerId=stackoverflow \
                                                         -s enabled=true \
                                                         -s 'config.useJwksUrl="true"' \
                                                         -s config.clientId=STACKAPPS_CLIENT_ID \
                                                         -s config.clientSecret=STACKAPPS_CLIENT_SECRET \
                                                         -s config.key=STACKAPPS_KEY

# User Federation
# necessite un ADDC présent !
#kcadm.sh create user-federation/instances -r demorealm -s providerName=kerberos -s priority=0 -s config.debug=false -s config.allowPasswordAuthentication=true -s 'config.editMode="UNSYNCED"' -s config.updateProfileFirstLogin=true -s config.allowKerberosAuthentication=true -s 'config.kerberosRealm="KEYCLOAK.ORG"' -s 'config.keyTab="http.keytab"' -s 'config.serverPrincipal="HTTP/localhost@KEYCLOAK.ORG"'
CONNECTION_ADDC=ldap://localhost:10389
kcadm.sh create components -r demorealm -s name=kerberos-ldap-provider -s providerId=ldap -s providerType=org.keycloak.storage.UserStorageProvider -s parentId=3d9c572b-8f33-483f-98a6-8bb421667867  -s 'config.priority=["1"]' -s 'config.fullSyncPeriod=["-1"]' -s 'config.changedSyncPeriod=["-1"]' -s 'config.cachePolicy=["DEFAULT"]' -s config.evictionDay=[] -s config.evictionHour=[] -s config.evictionMinute=[] -s config.maxLifespan=[] -s 'config.batchSizeForSync=["1000"]' -s 'config.editMode=["WRITABLE"]' -s 'config.syncRegistrations=["false"]' -s 'config.vendor=["other"]' -s 'config.usernameLDAPAttribute=["uid"]' -s 'config.rdnLDAPAttribute=["uid"]' -s 'config.uuidLDAPAttribute=["entryUUID"]' -s 'config.userObjectClasses=["inetOrgPerson, organizationalPerson"]' -s 'config.connectionUrl=['"$CONNECTION_ADDC"']'  -s 'config.usersDn=["ou=People,dc=keycloak,dc=org"]' -s 'config.authType=["simple"]' -s 'config.bindDn=["uid=admin,ou=system"]' -s 'config.bindCredential=["secret"]' -s 'config.searchScope=["1"]' -s 'config.useTruststoreSpi=["ldapsOnly"]' -s 'config.connectionPooling=["true"]' -s 'config.pagination=["true"]' -s 'config.allowKerberosAuthentication=["true"]' -s 'config.serverPrincipal=["HTTP/localhost@KEYCLOAK.ORG"]' -s 'config.keyTab=["http.keytab"]' -s 'config.kerberosRealm=["KEYCLOAK.ORG"]' -s 'config.debug=["true"]' -s 'config.useKerberosForPasswordAuthentication=["true"]'

# Triggering synchronization of all users for specific user storage provider
#kcadm.sh create user-storage/b7c63d02-b62a-4fc1-977c-947d6a09e1ea/sync?action=triggerFullSync

# Test LDAP user storage connectivity
kcadm.sh get testLDAPConnection -q action=testConnection -q bindCredential=secret -q bindDn=uid=admin,ou=system -q connectionUrl=ldap://localhost:10389 -q useTruststoreSpi=ldapsOnly

# Test LDAP user storage authentication
kcadm.sh get testLDAPConnection -q action=testAuthentication -q bindCredential=secret -q bindDn=uid=admin,ou=system -q connectionUrl=ldap://localhost:10389 -q useTruststoreSpi=ldapsOnly

# to set password policy to 20000 hash iterations, requiring at least one special character, at least one uppercase character, at least one digit character, not be equal to user’s username, and be at least 8 characters long you would use the following:
kcadm.sh update realms/demorealm -s 'passwordPolicy="hashIterations and specialChars and upperCase and digits and notUsername and length"'

# to set password policy to 25000 hash iterations, requiring at least two special characters, at least two uppercase characters, at least two lowercase characters, at least two digits, be at least nine characters long, not be equal to user’s username, and not repeat for at least four changes back:
kcadm.sh update realms/demorealm -s 'passwordPolicy="hashIterations(25000) and specialChars(2) and upperCase(2) and lowerCase(2) and digits(2) and length(9) and notUsername and passwordHistory(4)"'

kcadm.sh get events --offset 0 --limit 100



