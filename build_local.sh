#!/bin/bash

SCRIPT_PATH="${BASH_SOURCE[0]}"
if [ -h "${SCRIPT_PATH}" ]
then
  while [ -h "${SCRIPT_PATH}" ]
  do 
      SCRIPT_PATH=$(readlink "${SCRIPT_PATH}")
  done
fi
pushd . > /dev/null
DIR_SCRIPT=$(dirname "${SCRIPT_PATH}" )
cd "${DIR_SCRIPT}" > /dev/null || exit 1
SCRIPT_PATH=$(pwd);
popd  > /dev/null || exit 1

JAVA_HOME=/usr/lib/jvm/java-17-openjdk-amd64
export JAVA_HOME
PATH="$JAVA_HOME/bin:$PATH"
export PATH

if [ -z "$M2_HOME" ]
then
    M2_HOME=~/Data/.m2/
    if [ ! -d "$M2_HOME" ]
    then
        M2_HOME=~/.m2
    fi
fi

WORKSPACE=$SCRIPT_PATH
echo "WORKSPACE: $WORKSPACE"
echo "M2_HOME: $M2_HOME"
echo "JAVA_HOME: $JAVA_HOME"
echo "PATH: $PATH"
java --version
pushd . > /dev/null
cd "${WORKSPACE}" > /dev/null || exit 1

echo "*---------------------------------------------------------------------------------------------------------------------"
echo "* mvnw -e clean install"
./mvnw -e clean install 

echo "*---------------------------------------------------------------------------------------------------------------------"
echo "* mvnw versions:display-plugin-updates"
./mvnw versions:display-plugin-updates

echo "*---------------------------------------------------------------------------------------------------------------------"
echo "* mvn dependency:tree"
./mvnw dependency:tree

popd  > /dev/null || exit 1
