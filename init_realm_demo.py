import sys
import os
from keycloak import KeycloakAdmin
from keycloak_admin import get_groups, set_groups, create_user
from keycloak import KeycloakOpenIDConnection
from dotenv import load_dotenv

load_dotenv()
KEYCLOAK_URL = os.getenv("KEYCLOAK_URL")
KEYCLOAK_REALM = os.getenv("KEYCLOAK_REALM")
KEYCLOAK_USER_REALM = os.getenv("KEYCLOAK_USER_REALM")
KEYCLOAK_USERNAME = os.getenv("KEYCLOAK_USERNAME")
KEYCLOAK_PASSWORD = os.getenv("KEYCLOAK_PASSWORD")
KEYCLOAK_CLIENT_ID = os.getenv("KEYCLOAK_CLIENT_ID")

# Keycloak connection
keycloak_connection = KeycloakOpenIDConnection(
    server_url=KEYCLOAK_URL,
    user_realm_name=KEYCLOAK_USER_REALM,
    realm_name=KEYCLOAK_REALM,
    username=KEYCLOAK_USERNAME,
    password=KEYCLOAK_PASSWORD,
    client_id=KEYCLOAK_CLIENT_ID,
    verify=True)

keycloak_admin = KeycloakAdmin(connection=keycloak_connection)

def resetData():
    groups = keycloak_admin.get_groups()
    for gr in groups:
        print("Remove group: {}".format(gr['name']))
        #keycloak_admin.delete_group(gr['id'])

    users = keycloak_admin.get_users()
    for user in users:
        print("Remove user: {}".format(user['username']))
        #keycloak_admin.get_user(user.username)


def insertUser(user):

    emails = [{"address": user["emails"], "verified": True}]
    new_user = keycloak_admin.create_user({"email": user["emails"],
                                           "username": user["emails"],
                                           "enabled": True,
                                           "firstName": user["firstName"],
                                           "lastName": user["lastName"],
                                           "credentials": [{"value": user["password"], "type": "password", }]},
                                          exist_ok=False)

def insertStructure(structure):
    keycloak_admin.create_group({"name": structure["name"]})


def insertMailExtension(mailExtension):
    print("[{}] Insert mail extension: {}".format(
        datetime.now(), mailExtension["extension"]))
    db['asamextensions'].insert_one(mailExtension)

def configureRealmeAdmin():
    #######################################################
    #keycloak_admin.get realms
    
    #  "id" : "master",
    #  "realm" : "master",
    #  "displayName" : "Keycloak",
    #  "displayNameHtml" : "<div class=\"kc-logo-text\"><span>Keycloak</span></div>",
    #  "notBefore" : 0,
    #  "defaultSignatureAlgorithm" : "RS256",
    #  "revokeRefreshToken" : false,
    #  "refreshTokenMaxReuse" : 0,
    #  "accessTokenLifespan" : 60,
    #  "accessTokenLifespanForImplicitFlow" : 900,
    #  "ssoSessionIdleTimeout" : 1800,
    #  "ssoSessionMaxLifespan" : 36000,
    #  "ssoSessionIdleTimeoutRememberMe" : 0,
    #  "ssoSessionMaxLifespanRememberMe" : 0,
    #  "offlineSessionIdleTimeout" : 2592000,
    #  "offlineSessionMaxLifespanEnabled" : false,
    #  "offlineSessionMaxLifespan" : 5184000,
    #  "clientSessionIdleTimeout" : 0,
    #  "clientSessionMaxLifespan" : 0,
    #  "clientOfflineSessionIdleTimeout" : 0,
    #  "clientOfflineSessionMaxLifespan" : 0,
    #  "accessCodeLifespan" : 60,
    #  "accessCodeLifespanUserAction" : 300,
    #  "accessCodeLifespanLogin" : 1800,
    #  "actionTokenGeneratedByAdminLifespan" : 43200,
    #  "actionTokenGeneratedByUserLifespan" : 300,
    #  "oauth2DeviceCodeLifespan" : 600,
    #  "oauth2DevicePollingInterval" : 600,
    #  "enabled" : true,
    #  "sslRequired" : "external",
    #  "registrationAllowed" : false,
    #  "registrationEmailAsUsername" : false,
    #  "rememberMe" : false,
    #  "verifyEmail" : false,
    #  "loginWithEmailAllowed" : true,
    #  "duplicateEmailsAllowed" : false,
    #  "resetPasswordAllowed" : false,
    #  "editUsernameAllowed" : false,
    #  "bruteForceProtected" : false,
    #  "permanentLockout" : false,
    #  "maxFailureWaitSeconds" : 900,
    #  "minimumQuickLoginWaitSeconds" : 60,
    #  "waitIncrementSeconds" : 60,
    #  "quickLoginCheckMilliSeconds" : 1000,
    #  "maxDeltaTimeSeconds" : 43200,
    #  "failureFactor" : 30,
    #  "defaultRole" : {
    #    "id" : "4d329179-90f1-4b11-93e4-d2b66dae7232",
    #    "name" : "default-roles-master",
    #    "description" : "${role_default-roles}",
    #    "composite" : true,
    #    "clientRole" : false,
    #    "containerId" : "master"
    #  },
    #  "requiredCredentials" : [ "password" ],
    #  "otpPolicyType" : "totp",
    #  "otpPolicyAlgorithm" : "HmacSHA1",
    #  "otpPolicyInitialCounter" : 0,
    #  "otpPolicyDigits" : 6,
    #  "otpPolicyLookAheadWindow" : 1,
    #  "otpPolicyPeriod" : 30,
    #  "otpSupportedApplications" : [ "FreeOTP", "Google Authenticator" ],
    #  "webAuthnPolicyRpEntityName" : "keycloak",
    #  "webAuthnPolicySignatureAlgorithms" : [ "ES256" ],
    #  "webAuthnPolicyRpId" : "",
    #  "webAuthnPolicyAttestationConveyancePreference" : "not specified",
    #  "webAuthnPolicyAuthenticatorAttachment" : "not specified",
    #  "webAuthnPolicyRequireResidentKey" : "not specified",
    #  "webAuthnPolicyUserVerificationRequirement" : "not specified",
    #  "webAuthnPolicyCreateTimeout" : 0,
    #  "webAuthnPolicyAvoidSameAuthenticatorRegister" : false,
    #  "webAuthnPolicyAcceptableAaguids" : [ ],
    #  "webAuthnPolicyPasswordlessRpEntityName" : "keycloak",
    #  "webAuthnPolicyPasswordlessSignatureAlgorithms" : [ "ES256" ],
    #  "webAuthnPolicyPasswordlessRpId" : "",
    #  "webAuthnPolicyPasswordlessAttestationConveyancePreference" : "not specified",
    #  "webAuthnPolicyPasswordlessAuthenticatorAttachment" : "not specified",
    #  "webAuthnPolicyPasswordlessRequireResidentKey" : "not specified",
    #  "webAuthnPolicyPasswordlessUserVerificationRequirement" : "not specified",
    #  "webAuthnPolicyPasswordlessCreateTimeout" : 0,
    #  "webAuthnPolicyPasswordlessAvoidSameAuthenticatorRegister" : false,
    #  "webAuthnPolicyPasswordlessAcceptableAaguids" : [ ],
    #  "browserSecurityHeaders" : {
    #    "contentSecurityPolicyReportOnly" : "",
    #    "xContentTypeOptions" : "nosniff",
    #    "xRobotsTag" : "none",
    #    "xFrameOptions" : "SAMEORIGIN",
    #    "xXSSProtection" : "1; mode=block",
    #    "contentSecurityPolicy" : "frame-src 'self'; frame-ancestors 'self'; object-src 'none';",
    #    "strictTransportSecurity" : "max-age=31536000; includeSubDomains"
    #  },
    #  "smtpServer" : { },
    #  "eventsEnabled" : false,
    #  "eventsListeners" : [ "jboss-logging" ],
    #  "enabledEventTypes" : [ ],
    #  "adminEventsEnabled" : false,
    #  "adminEventsDetailsEnabled" : false,
    #  "identityProviders" : [ ],
    #  "identityProviderMappers" : [ ],
    #  "internationalizationEnabled" : false,
    #  "supportedLocales" : [ ],
    #  "browserFlow" : "browser",
    #  "registrationFlow" : "registration",
    #  "directGrantFlow" : "direct grant",
    #  "resetCredentialsFlow" : "reset credentials",
    #  "clientAuthenticationFlow" : "clients",
    #  "dockerAuthenticationFlow" : "docker auth",
    #  "attributes" : { },
    #  "userManagedAccessAllowed" : false,
    #  "clientProfiles" : {
    #    "profiles" : [ ]
    #  },
    #  "clientPolicies" : {
    #    "policies" : [ ]
    #  }
    
    print ("active France !")
    keycloak_admin._get_realm()  update realms/"$KEYCLOAK_REALM" -s internationalizationEnabled=true \
                                             -s 'supportedLocales=[ "en", "fr" ]' \
                                             -s defaultLocale=fr
    
    print ("active registrationAllowed")
    keycloak_admin.update realms/"$KEYCLOAK_REALM" -s registrationAllowed=true \
                                             -s rememberMe=true
    
    print ("active logs FULL !!!")
    keycloak_admin.update events/config -r "$KEYCLOAK_REALM" -s adminEventsEnabled=true \
                                                       -s adminEventsDetailsEnabled=true \
                                                       -s eventsEnabled=true \
                                                       -s eventsExpiration=172800 \
                                                       -s 'enabledEventTypes=["LOGIN_ERROR","REGISTER_ERROR","LOGOUT_ERROR","CODE_TO_TOKEN_ERROR","CLIENT_LOGIN_ERROR","FEDERATED_IDENTITY_LINK_ERROR","REMOVE_FEDERATED_IDENTITY_ERROR","UPDATE_EMAIL_ERROR","UPDATE_PROFILE_ERROR","UPDATE_PASSWORD_ERROR","UPDATE_TOTP_ERROR","VERIFY_EMAIL_ERROR","REMOVE_TOTP_ERROR","SEND_VERIFY_EMAIL_ERROR","SEND_RESET_PASSWORD_ERROR","SEND_IDENTITY_PROVIDER_LINK_ERROR","RESET_PASSWORD_ERROR","IDENTITY_PROVIDER_FIRST_LOGIN_ERROR","IDENTITY_PROVIDER_POST_LOGIN_ERROR","CUSTOM_REQUIRED_ACTION_ERROR","EXECUTE_ACTIONS_ERROR","CLIENT_REGISTER_ERROR","CLIENT_UPDATE_ERROR","CLIENT_DELETE_ERROR"]' 

def configureRealmeAdminEmail():
    #######################################################
    #keycloak_admin.get users
    #{
    #  "id" : "116db245-bd1c-4f7b-bbf4-03d78b09e1ed",
    #  "createdTimestamp" : 1695370109413,
    #  "username" : "admin",
    #  "enabled" : true,
    #  "totp" : false,
    #  "emailVerified" : false,
    #  "email" : "gilles.grandgerard@ac-dijon.fr",
    #  "disableableCredentialTypes" : [ ],
    #  "requiredActions" : [ ],
    #  "notBefore" : 0,
    #  "access" : {
    #    "manageGroupMembership" : true,
    #    "view" : true,
    #    "mapRoles" : true,
    #    "impersonate" : true,
    #    "manage" : true
    #  }
    
    print ("active email")
    USER_UID=$(getUserId "$KEYCLOAK_USER")
    print ("UID for '$KEYCLOAK_USER' = $USER_UID"
    
    #$keycloak_admin update "users/$USER_UID/reset-password" -r "$KEYCLOAK_REALM" -s type=password -s value="$PASSWORD" -s temporary=false -n
    
    keycloak_admin.update users/"$USER_UID" -r "$KEYCLOAK_REALM" -s 'email=gilles.grandgerard@ac-dijon.fr' \
                                                                       -s 'emailVerified=true'



if(len(sys.argv) >= 2):
    reset = sys.argv[1]

    if reset == '-r':
        resetData()

